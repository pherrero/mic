
function p18()

    % variables
    var{1} = {'x1',[-1,1]};
    var{2} = {'x2',[-1,1]};
    var{3} = {'x3',[-1,1]};
    var{4} = {'x4',[-1,1]};
    var{5} = {'x5',[-1,1]};
    var{6} = {'x6',[-1,1]};
    var{7} = {'x7',[-1,1]};
    var{8} = {'x8',[-1,1]};
    
    % function
    f = 'x1*x2*x3*x4*x5*x6*x7 + x2*x3*x4*x5*x6*x7*x8 + x1*x3*x4*x5*x6*x7*x8 + x1*x2*x4*x5*x6*x7*x8 + x1*x2*x3*x5*x6*x7*x8 + x1*x2*x3*x4*x6*x7*x8 + x1*x2*x3*x4*x5*x7*x8 + x1*x2*x3*x4*x5*x6*x8';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon
    param.time = 100;     % Time limit for the computation
    param.simpl = 0;
    
     % call mic
    [inn,out] = fstar(var,f,param);

end