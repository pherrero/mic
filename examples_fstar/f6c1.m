% Example f6c1

function f6c1()

    % variables
     var{1} = {'x1',[1,3]};
     var{2} = {'x2',[10,20]};
     var{3} = {'x3',[-3,1]};
     var{4} = {'x4',[1,0]};
     var{5} = {'x5',[2,0.5]};
    
    % function
    f = '2.0*x4*x5+(x1+x4)/(x3+x1*x2)';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon

    [inn,out] = fstar(var,f,param);
    
end 