

f_aux ='-(ix(1,:)*a122+a121*da112-da112*da121)/(ix(2,:)*(ix(3,:)*a121-ix(2,:)*da122))';
expression ='da112';
[iex, eex] = regexp(f_aux,expression)

for k=1:numel(iex)
   expression ='da112';
   if iex(k)>1
    [i, e] = regexp(f_aux(iex(k)-1),'\w');
    if ~isempty(i)
        iex(k)=[];
        eex(k)=[];
    end
   elseif eex(k)<numel(f_aux)
    [i, e] = regexp(f_aux(eex(k)+1),'\w');   
    if ~isempty(i)
        iex(k)=[];
        eex(k)=[];
    end
   end
end


%         ind = [];
%         for k=1:numel(iex)
%            if iex(k)>1
%             [i, e] = regexp(f_aux(iex(k)-1),'\w');
%             if ~isempty(i)
%                 ind(numel(ind)+1)=k;
%             end
%            elseif eex(k)<numel(f_aux)
%             [i, e] = regexp(f_aux(eex(k)+1),'\w');   
%             if ~isempty(i)
%                 ind(numel(ind)+1)=k;
%             end
%            end
%         end
%             iex(ind)=[];
%             eex(ind)=[];