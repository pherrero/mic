% VARS = [];
% VARS{numel(VARS)+1} = {'a',[1,3]};
% VARS{numel(VARS)+1} = {'b',[1,2]};
% VARS{numel(VARS)+1} = {'c',[3,1]};
% VARS{numel(VARS)+1} = {'f',[12,83]};
% fx = '(f-dual(a*c+b^2))/dual(a^2*b+c)';
% 
% parameters
param.tol = 1e-3;     % Tolerance
param.eps = 1e-3;     % Epsilon
param.print = true;
param.iter = 10;

% [innx,outx] = fstar(VARS,fx,param)

VARS = [];
VARS{numel(VARS)+1} = {'a',[1,3]};
VARS{numel(VARS)+1} = {'b',[1,2]};
VARS{numel(VARS)+1} = {'c',[3,1]};
VARS{numel(VARS)+1} = {'f',[12,83]};
fx = '(f-(dual(a)*dual(c)+dual(b)^2))/(dual(a)^2*dual(b)+dual(c))';
[innx,outx] = fstar(VARS,fx,param)

% VARS = [];
% VARS{numel(VARS)+1} = {'a',[3,1]};
% VARS{numel(VARS)+1} = {'b',[2,1]};
% VARS{numel(VARS)+1} = {'c',[1,3]};
% VARS{numel(VARS)+1} = {'f',[83,12]};
% fx = '(f-(a*c+b^2))/(a^2*b+c)';
% [innx,outx] = fstar(VARS,fx,param)