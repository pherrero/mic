function f1()

% parameters
param.tol = 1e-3;     % Tolerance
param.eps = 1e-3;     % Epsilon
param.print = true;
param.iter = 10;

% variables
var{1} = {'u',[0,6]};
var{2} = {'v',[8,2]};
var{3} = {'x',[8,2]};

% function
f = 'u^2+v^2+2*u*v-20*u-20*v+100';
% call mic
[inn,out] = fstar(var,f,param);

   
end
    




