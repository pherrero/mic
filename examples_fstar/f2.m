
function f2()

    % variables
    var{1} = {'x1',[0,6]};
    var{2} = {'x2',[8,2]};
    
    % function
     f = '4*x1^2-2.1*x1^4+x1^6/3+x1*x2-4*x2^2+4*x2^4';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon
    param.time = 10;     % Time limit for the computation
    param.simpl = 0;
    
    [inn,out] = fstar(var,f,param);

end