
function f5()

    % variables
    var{1} = {'x1',[4,0]};
    var{2} = {'x2',[2,8]};
    var{3} = {'x3',[-4,9]};
    
    % function
    f = 'x3^2-9.8*x2/(1.0+x2*x1)';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.time = 10;     % Time limit for the computation
    param.simpl = 1;
    
    [inn,out] = fstar(var,f,param);

end
