function ae_qrc()

% variables
var{1} = {'u',[0,6]};
var{2} = {'v',[8,2]};
var{3} = {'z',[9,-4]};

% function
f = 'u^2+v^2+2*u*v-20*u-20*v+100-10*sin(z)';

% parameters
param.tol = 1e-3;     % Tolerance
param.eps = 1e-3;     % Epsilon
param.inex = {{'out','inn'}',{'inc','ninc'},[0,0]};

% call fstar
[inn,out] = fstar(var,f,param);

end
    




