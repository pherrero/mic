function p4()

    % variables
    var{1} = {'x1',[-0.5,0.5]};
    var{2} = {'x2',[-0.5,0.5]};
    var{3} = {'x3',[-0.5,0.5]};
    var{4} = {'x4',[-0.5,0.5]};
    
    % function
    f = '-x1*x3^3 + 4*x2*x3^2*x4 + 4*x1*x3*x4^2 + 2*x2*x4^3 + 4*x1*x3 + 4*x3^2 - 10*x2*x4 - 10*x4^2 + 2';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon
    param.simpl = 0;
    
    [inn,out] = fstar(var,f,param);

end