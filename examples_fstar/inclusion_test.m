function inclusion_test()

% variables
var{1} = {'u',[0,6]};
var{2} = {'v',[2,8]};

% function
f = 'u^2+v^2+2*u*v-20*u-20*v+100';

% parameters
param.tol = 1e-4;     % Tolerance
param.eps = 1e-4;     % Epsilon
param.print = true;
param.iter = 10;
param.inex = {'inn','int',[-1,60]};

% call mic
[inn,out] = fstar(var,f,param);

end