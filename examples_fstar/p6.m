
function p6()

    % variables
    var{1} = {'x1',[-1,1]};
    var{2} = {'x2',[-1,1]};
    var{3} = {'x3',[-1,1]};
    var{4} = {'x4',[-1,1]};
    var{5} = {'x5',[-1,1]};
    
    % function
    f = '-1 + 2*x1^6 - 2*x2^6 + 2*x3^6 - 2*x4^6 + 2*x5^6';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.simpl = 1;
    
     % call mic
    [inn,out] = fstar(var,f,param);

end