
function p14()

%     % variables
var{1} = {'x1',[-10,10]};
var{2} = {'x2',[-10,10]};
var{3} = {'x3',[-10,10]};
var{4} = {'x4',[-10,10]};
var{5} = {'x5',[-10,10]};
var{6} = {'x6',[-10,10]};
var{7} = {'x7',[-10,10]};

% function
f = 'x1^2 + 2*x2^2 + 2*x3^2 + 2*x4^2 + 2*x5^2 + 2*x6^2 + 2*x7^2 - x1';

% parameters
param.tol = 1e-6;    % Tolerance
param.eps = 1e-6;     % Epsilon
param.simpl = 1;

% call mic
[inn,out] = fstar(var,f,param);

end
    
