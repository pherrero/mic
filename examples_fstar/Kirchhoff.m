%% Kirchhoff Voltage Law Demo
function Kirchhoff()

% Resitors
R1 = 20;
R2 = 5;
R3 = 10;

% Tolerance resistors
T1 = 0.1;
T2 = 0.1;
T3 = 0.1;

% Voltage source
V1 = 15;
V2 = 7;

% Tolerance voltage source
Tv1 = 0.05;
Tv2 = 0.05;

% Variables
var{1} = {'V1',[V1-Tv1*V1,V1+Tv1*V1]};
%var{2} = {'V2',[V2-Tv2*V2,V2+Tv2*V2]}; % fixed voltage source
var{2} = {'V2',[10,0]};               % variable voltage source
var{3} = {'R1',[R1-T1*R1, R1+T1*R1]};
var{4} = {'R2',[R2-T2*R2, R2+T2*R2]};
var{5} = {'R3',[R3-T3*R3, R3+T3*R3]};

% parameters
param.tol = 0.01;       % Tolerance
param.eps = 0.01;       % Epsilon
param.time = 100;       % Time limit for the computation
param.simpl = 0;        % Simplify objective function (0 = No simplify; 1 = Simplify)
param.iter = 5;      	% Partial results display
%param.jm = true;      	% Partial results display

% % I1
% f = '(V2*R3+V1*(R2+R3))/((R2+R3)*(R3+R1)-R3^2)'; 
% 
% [inn,out] = fstar(var,f,param);

% I2
f = '(((V2*R3+V1*(R2+R3))/((R2+R3)*(R3+R1)-R3^2))*(R3+R1)-V1)/R3'; 

[inn,out] = fstar(var,f,param);


end