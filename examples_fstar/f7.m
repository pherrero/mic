
function f7()

    % variables
    var{1} = {'x1',[1,3]};
    var{2} = {'x2',[10,20]};
    var{3} = {'x3',[1,3]};
    var{4} = {'x4',[1,0]};
    var{5} = {'x5',[2,0.5]};
    var{6} = {'x6',[2,0.5]};
    var{7} = {'x7',[2,0.5]};
    
    % function
    f= 'sqrt((2.0*x6*x7-x2+2.0*x3+x4+2.0*x5)^2+(x7^2-(x3+x4+x5)*(x3+x5-x2)+x4*x5*(x4*x3^2-x1))^2)';

    % parameters
    param.tol = 0.01;    % Tolerance
    param.eps = 0.01;     % Epsilon
    param.time = 100;     % Time limit for the computation
    
    [inn,out] = fstar(var,f,param);
    
end
