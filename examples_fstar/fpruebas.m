function [var,f,param] = fpruebas()
clc
tic
clear all
warning off
format long

% parameters
     param.tol = 0.0001;    % Tolerance
     param.eps = 0.0001;     % Epsilon
     param.time = 30;     % Time limit for the computation
     param.print = true;
     %param.print = false;
     param.simpl = 1;      % Simplify objective function (0 = No  simplify; 1 = Simplify)
     param.iter = 1;       %Resultados parciales por pantalla cada  "param.iter" iteraciones


%    A=[interval(2.5,2.1)    interval(3.1,3.5)    interval(0.0,0.0);
%       interval(3.1,3.5)   interval(-4.6,-4.6)   interval(1.0,1.0)];
%    [nf,nc]=size(A);
% 
%    VARS = [];
%    for i=1:nf
%      for j=1:nc
%        VARS{numel(VARS)+1} = {['a1' num2str(i) num2str(j)],A(i,j)};
%        VARS{numel(VARS)+1} = {['adu1' num2str(i) num2str(j)],dual(A(i,j))};
%      end
%    end
% 
%    fx='a111+a111-a111'
%    [innx,outx] = fstar(VARS,fx,param)

   A=[interval(2.5,2.1)    interval(3.1,3.5)    interval(0.0,0.0);
      interval(3.1,3.5)   interval(-4.6,-4.6)   interval(1.0,1.0)];
   [nf,nc]=size(A);

   VARS = [];
   for i=1:nf
     for j=1:nc
       VARS{numel(VARS)+1} = {['a1' num2str(i) num2str(j)],A(i,j)};
       VARS{numel(VARS)+1} = {['adu1' num2str(i) num2str(j)],dual(A(i,j))};
     end
   end

fx  ='(a113*(a122*a111-adu112*adu121)-(adu123*adu111-a113*a121)*adu112)/(adu111*(adu122*adu111-a112*a121))'

   [innx,outx] = fstar(VARS,fx,param)

toc
end