function p1()

% variables
var{1} = {'x1',[-1.5,2]};
var{2} = {'x2',[-1.5,2]};
var{3} = {'x3',[-1.5,2]};

% function
f = 'x1*x2^2 + x1*x3^2 - 1.1*x1 + 1';

% parameters
param.tol = 1e-6;    % Tolerance
param.eps = 1e-6;     % Epsilon
param.simpl = 1;

[inn,out] = fstar(var,f,param);

end
