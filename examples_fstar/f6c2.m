function f6c2()

    % variables
    var{1} = {'x1',[-3,-2.8]};
    var{2} = {'x2',[9.1,50]};
    var{3} = {'x3',[4,.5]};
    
    % function
    f = 'x3^2-9.8*x1/(1+x1*x2)';
    
    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    
    [inn,out] = fstar(var,f,param);

end
