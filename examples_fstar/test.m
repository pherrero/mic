function test()


% parameters
param.tol = 1e-2;     % Tolerance
param.eps = 1e-5;     % Epsilon
param.print = true;
param.iter = 10;

VARS = [];
 VARS{numel(VARS)+1} = {'x',[3,8]};
 VARS{numel(VARS)+1} = {'y',[4,9]};
 fR = 'join(x,y)'
 [innR,outR] = fstar(VARS,fR,param);
 res=interval(outR.lower,outR.upper)

end
    


