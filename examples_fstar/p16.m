
function p16()

    % variables
    var{1} = {'x1',[-0.1,0.4]};
    var{2} = {'x2',[0.4,0.1]};
    var{3} = {'x3',[-0.7,-0.4]};
    var{4} = {'x4',[-0.7,-0.4]};
    var{5} = {'x5',[0.1,0.2]};
    var{6} = {'x6',[-0.1,0.2]};
    var{7} = {'x7',[-0.3,1.1]};
    var{8} = {'x8',[-1.1,-0.3]};
    
    % function
    f = 'x1*x6^3 -3*x1*x6*x7^2 + x3*x7^3 - 3*x3*x7*x6^2 + x2*x5^3 - 3*x2*x5*x8^2 + x4*x8^3 - 3*x4*x8*x5^2 + 0.9563453';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.simpl = 1;
    
     % call mic
    [inn,out] = fstar(var,f,param);

end