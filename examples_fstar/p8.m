
function p8()

    %     % variables
    var{1} = {'x1',[-10,10]};
    var{2} = {'x2',[-10,10]};
    var{3} = {'x3',[-10,10]};
    var{4} = {'x4',[-10,10]};
    var{5} = {'x5',[-10,10]};

    % function
    f = 'x1*x2*x3*x4 + x1*x2*x3*x5 + x1*x2*x4*x5 + x1*x3*x4*x5  + x2*x3*x4*x5';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-4;     % Epsilon
    param.simpl = 1;

    % call mic
    [inn,out] = fstar(var,f,param);

end
    
