function multiple_functions()

% variables
vars{1} = {'u',[0,6]};
vars{2} = {'v',[2,8]};

% functions
f1 = 'u^2+v^2+2*u*v-20*u-20*v+100';
f2 = 'u^3+v^3+2*u*v-20*u-20*v+100';

param.tol = 1e-4;     % Tolerance
param.eps = 1e-4;     % Epsilon


% symbolic routine
[x1, fs1] = fstar_symbolic(f1,vars, param);
[x2, fs2] = fstar_symbolic(f2,vars, param);

% numeric routine
[Inn_1,Out_1] = fstar_numeric(x1, fs1, param);
[Inn_2,Out_2] = fstar_numeric(x2, fs2, param);

% change intervals
x1(1) = interval([3,9]);
x1(2) = interval([6,2]);

% numeric routine
[Inn_11,Out_11] = fstar_numeric(x1, fs1, param);

% change intervals
x2(1) = interval([-5,5]);
x2(2) = interval([0,4]);

% numeric routine
[Inn_22,Out_22] = fstar_numeric(x2, fs2, param);

end