
function p17()

    % variables
    var{1} = {'x1',[-1,1]};
    var{2} = {'x2',[-1,1]};
    var{3} = {'x3',[-1,1]};
    var{4} = {'x4',[-1,1]};
    var{5} = {'x5',[-1,1]};
    var{6} = {'x6',[-1,1]};
    var{7} = {'x7',[-1,1]};
    var{8} = {'x8',[-1,1]};
    
    % function
    f = '-2*x1*x4 + 2*x1*x7 - 2*x2*x5 + 2*x2*x7 - 2*x3*x6 + 2*x3*x7 + 2*x4*x7 + 2*x5*x7 + 8*x6*x7 - 6*x6*x8 + 8*x7^2 + 6*x7*x8 - x7';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.time = 100;     % Time limit for the computation
    param.simpl = 1;
    
     % call mic
    [inn,out] = fstar(var,f,param);

end