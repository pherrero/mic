function p3()

    % variables
    var{1} = {'x1',[-2,2]};
    var{2} = {'x2',[-2,2]};
    var{3} = {'x3',[-2,2]};
    var{4} = {'x4',[-2,2]};
    
    % function
    f = 'x1*x2^2 + x1*x3^2 + x1*x4^2 - 1.1*x1 + 1';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon
    param.simpl = 1;

    [inn,out] = fstar(var,f,param);

end