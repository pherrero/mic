
function f8()

%     % variables
    var{1} = {'x',[1,2]};
    var{2} = {'y',[1,3]};
    var{3} = {'z',[3,4]};
    f = 'x*y-x*y+z';  
    
    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    
    [inn,out] = fstar(var,f,param);

end