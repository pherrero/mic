function p7()

%     % variables
    var{1} = {'x1',[0,1]};
    var{2} = {'x2',[2,3]};
    var{3} = {'x3',[-2,-1]};
    var{4} = {'x4',[1,3]};
    var{5} = {'x5',[-2,-1]};

    % function
    f = 'x2^6*x3 + x2*x3^6 + x1^2*x2^4*x5 - 3*x1*x2^2*x3^2*x4*x5 + x3^4*x4^2*x5 - x1^3*x3*x4*x5^2 - x1*x2*x4^3*x5^2 + x2*x3*x5^5';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon

    [inn,out] = fstar(var,f,param);

end