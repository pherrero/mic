function p5()

    % variables
    var{1} = {'x1',[-5,5]};
    var{2} = {'x2',[-5,5]};
    var{3} = {'x3',[-5,5]};
    var{4} = {'x4',[-5,5]};
    var{5} = {'x5',[-5,5]};
    
    % function
    f = 'x5^2 + x1 + x2 + x3 +x4 - x5 -10';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon

    [inn,out] = fstar(var,f,param);
    
end