function p2()

    % variables
    var{1} = {'x1',[-5,5]};
    var{2} = {'x2',[-5,5]};
    var{3} = {'x3',[-5,5]};
    
    % function
    f = 'x1-2*x2+x3+0.835634534*x2*(1-x2)';

    % parameters
    param.tol = 1e-6;    % Tolerance
    param.eps = 1e-6;     % Epsilon

    [inn,out] = fstar(var,f,param);
    
end