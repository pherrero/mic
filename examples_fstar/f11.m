function f11()

var{1} = {'a',[0.1 0.2]};
var{2} = {'b',[0.3 0.4]};
var{3} = {'x',[1.5 ,1.56]};
var{4} = {'y',[.75 .70]};

%function
f = '(y-(a*x+b))^2'

% parameters
param.tol = 0.0001;   % Tolerance
param.eps = 0.0001;   % Epsilon

[inn,out] = fstar(var,f,param);

end