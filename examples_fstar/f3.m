function f3()

    % variables
    var{1} = {'x1',[0,6]};
    var{2} = {'x2',[8,2]};
    
    % function
     f = 'x2^2*(2+x1*x2)';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.time = 10;     % Time limit for the computation
    
    [inn,out] = fstar(var,f,param);

end
