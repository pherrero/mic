
function f4()

    % variables
    var{1} = {'x1',[4,0]};
    var{2} = {'x2',[2,8]};
    var{3} = {'x3',[-4,9]};
    var{4} = {'x4',[3,-1]};
    
    % function
    f = 'x1^2+(x1+x2)^2+(x1+x2+x3)^2+(x1+x2+x3+x4)^2';

    % parameters
    param.tol = 1e-3;    % Tolerance
    param.eps = 1e-3;     % Epsilon
    param.time = inf;     % Time limit for the computation
    %param.simpl = 1;
    
    [inn,out] = fstar(var,f,param);

end
