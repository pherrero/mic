function f6()

    % variables
    var{1} = {'x1',[1,3]};
    var{2} = {'x2',[10,20]};
    var{3} = {'x3',[1,3]};
    var{4} = {'x4',[1,0]};
    var{5} = {'x5',[2,0.5]};
    var{6} = {'x6',[2,0.5]};
    
    % function
    f = 'sqrt((2.0*x4*x5+(x3+x4)/(x2+x3*x1))^2+(x6^2-9.8*x3/(1.0+x3*x2))^2)';
    
    % parameters
    param.tol = 0.005;    % Tolerance
    param.eps = 0.001;     % Epsilon
    param.time = 100;     % Time limit for the computation
    param.simpl = 0;

    [inn,out] = fstar(var,f,param);
    
end
