function f10()

  %variables
   var{1} = {'x',[-1,2]};
   var{2} = {'y',[3,4]};
   var{3} = {'z',[1,3]};
   
   %function
   f='x*y-x*z+z'
   
   % parameters
   param.tol = 1e-4;    % Tolerance
   param.eps = 1e-4;     % Epsilon
   
   [inn,out] = fstar(var,f,param);

end