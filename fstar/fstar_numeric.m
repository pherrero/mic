
%% MIC V1.0
% Authors: Pau Herrero (1) and M.A. Sainz (2)
% Coded by Pau Herrero
% (1) Center for Bio-Inspired Technology, Department of Electrical and
% Electronic Engineering Imperial College London. pherrero@imperial.ac.uk
% (2) Institut d'Insformarica i Aplicacions, Universitat de Girona
% 07/05/2015
%
% GNU General Public License:
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Numeric Part
function [Inner,Outer] = fstar_numeric(x, fs, param)

% Default parameters if they do not exist
if ~isfield(param,{'eps'})
    param.eps = 1e-3;
end
if ~isfield(param,{'tol'})
    param.tol = 1e-3;
end
if ~isfield(param,{'iter'})
    param.iter = 10;
end
if ~isfield(param,{'time'})
    param.time = inf;
end
if ~isfield(param,{'print'})
    param.print = true;
end 
if ~isfield(param,{'jm'})
    param.jm = false;
end

%% Extract proper and improper index
fs.pv = find(isprop(x)); % proper variables index
fs.iv = find(isimpr(x) | ispunct(x)); % improper variables index

%% Apply dual to variables if no improper variable (for performance reasons)
%if (numel(fs.pv) > numel(fs.iv)) || (numel(fs.pv) == numel(fs.iv)) && nnz(fs.num_incis(fs.pv)==1) < nnz(fs.num_incis(fs.iv)==1)
if numel(fs.iv)==0 || param.jm
    x = dual(x);
    aux = fs.pv;
    fs.pv = fs.iv;
    fs.iv = aux;
    fs.dual_flag = 1;
else
    fs.dual_flag = 0;
end

if param.print
    disp(' ')
    disp(['Running f-star...'])
end

w0 = width(x);
w0(w0==0) = -inf;           % variables that its initial width is zero.

eps_rel = ones(numel(w0),1)*param.eps;   % compute relative epsilon

[mon] = evaluate_monotony(x,fs.max_x_incis,fs); % evaluate monotony
if ~isempty(fs.pv)
    monpv = (abs(prod(mon,3)) == 1);
    monpv = monpv(:,1);
else 
    monpv = [];
end

[Inner,Outer] = obj_func(x,mon,monpv,fs); % compute inner and outer approximation

TOL = tol(Inner,Outer);

uni_topt_xp = (fs.num_incis(fs.pv) ~= 1 | fs.tree_opt ~= 1);    % uniincident proper variables and tree-optimality
uni_topt_xi = (fs.num_incis(fs.iv) ~= 1 | fs.tree_opt ~= 1); % uniincident improper variables and tree-optimality

numvars = numel(eps_rel);

% Initialization
strip.x = x;
strip.cinn = Inner; 
strip.cout = Outer;
strip.inn = Inner;
strip.out = Outer;
strip.mon = mon;
strip.index = true;
strip.bisect = true;
strip.ncells = 1;
list(1) = strip; % add strip to list

% Main loop
iter = 0;       % iteration counter
etime = 0;
itime = cputime;
while (TOL>param.tol || isnan(TOL)) && etime < param.time 
    
    % break if NaN
    if isnan(Inner.lower) || isnan(Inner.upper) || isnan(Outer.lower) || isnan(Outer.upper)
        break
    end

    % inclusion/exclusion stop
    if isfield(param,{'inex'})
       Dual = fs.dual_flag;
       InnOut = param.inex{1};
       IncExc = param.inex{2};
       Interval = interval(param.inex{3});
       stop = inclusion_exclusion(Inner,Outer,Dual,InnOut,IncExc,Interval);
       if stop
           if param.print
               disp('Terminate due to inclusion/exclusion')
               disp('');
           end
           break
       end
    end
    
    
    %% Eliminate redundant strips and select strip tp bisect
    SSout = [list(:).out]; % strips outer approximations
    index = isin(SSout,Inner);
    list(index) = [];
    SSinn = [list(:).inn]; % strips inner approximations
    SSout = [list(:).out]; % strips outer approximations
    
    SBisect = [list(:).bisect];         % strips to bisect
    if nnz(SBisect) == 0                % break is no strips to bisect
        break
    end  
    
    %[~, IS] = max(tol(SSinn,SSout) .* SBisect); % Strip to bisect
    
    TolS = tol(SSinn,SSout) .* SBisect;
    [~, OStrips] = sort(TolS,'descend');

    if numel(fs.pv)==0;
        nloop = 1;
    else
        nloop = numel(fs.pv);
    end
    
    for k=1:min(nloop,numel(OStrips))
        
       IS = OStrips(k);

        %% Max width of non-monotonic (along the strip) proper variables
        if ~isempty(fs.pv)
            monxp = list(IS).mon(fs.pv,1,:);  % monotonic proper variables 
            monxps = (abs(prod(monxp,3)) == 1); % monotonic proper variables along the strip
            mxp = find((monxps==0 & uni_topt_xp')); % nono-monotonic proper intervals and uni-incident variables in a non-optimal tree   
            xp = list(IS).x(fs.pv,1);          % proper intervals
            wxp = width(xp(mxp))./w0(fs.pv(mxp));           % width of intervals
            [wxp_max, ixp_max] = max(wxp);
            if isempty(wxp_max) || (wxp_max <= eps_rel(fs.pv(ixp_max))) wxp_max=0; end
        else
            wxp_max=0;
        end

        %% Index to cells to be bisected based on condition Sout \in CSinn  
        CSinn = list(IS).cinn;      % cells inner approximations
        CSout = list(IS).cout;      % cells inner approximations
        Sout = list(IS).out;      % strip outer approximations

        index = ~isin(Sout,CSinn) & list(IS).index; % Bounding criteris Not bisect when Out(Strip) in Inn(Cell) 

        if nnz(index) == 0              % Do not bisect the the strip
            list(IS).bisect = false;
        end

        N = 1000;
        if nnz(index)>N
            Ctol = tol(CSinn(index),CSout(index)); 
            [~, I1] = sort(Ctol,'descend');
            [~, I2] = find(index);
            index1 = false(1,list(IS).ncells);
            index1(I2(I1(1:N)))=true;  
        else
            index1 = index;
        end 

        %% Max width of non-monotonic improper variables
        x = list(IS).x(:,index1); 
        mon = list(IS).mon(:,1,index1); 
        mon = squeeze(mon==1); 
        if (numel(fs.iv) == 1 && isempty(fs.pv)) % particular case when iv=1 due to squeeze (size(mon) ~= size(xi)) 
            mon = mon'; 
        end         
        wx = bsxfun(@rdivide,width(x),w0);

        nnzi = nnz(index1);                    % number of cells to be bisected
        xi = false(numvars,nnzi);
        xi(fs.iv(uni_topt_xi),:) = true;

        wxm = wx .* ~mon .* xi;
        [wxi, mxi] = max(wxm,[],1);

        xmax = false(size(wx));
        xmax(sub2ind(size(xmax),mxi,1:numel(mxi))) = true;

        epsb = wx >= param.eps;

        wi = ~mon & epsb & xmax;

        %% Index to improper variables that are not uni-unicident+tree-optimality

        wxi_max = max(wxi);

        wi = wi & xi; % Bisect only improper variables

        if ~isempty(x(wi)) && (wxi_max >= wxp_max)      % Condition to decide which variable to bisect 

            [L,R] = bis(x(wi)); % bisect improper intervals

            xaux = x;
            x(wi) = L;
            xaux(wi) = R;

            x = [x xaux];
            TC = nnz(index1);
            index2 = [index1 true(1,TC)];
            BC = nnz(index2); % number of bisected cells  

            [mon] = evaluate_monotony(x,fs.max_x_incis,fs); % evaluate monotony
            monpv = (abs(prod(mon,3)) == 1);
            monpv = monpv(:,1);
            monpv = repmat(monpv,1,BC);

            [inn,out] = obj_func(x,mon,monpv,fs);  % compute inner and outer approximation

            inn = interval(squeeze(inn.lower),squeeze(inn.upper))';
            out = interval(squeeze(out.lower),squeeze(out.upper))';

            list(IS).x(:,index2) = x;
            list(IS).cinn(index2) = inn;
            list(IS).cout(index2) = out;     
            list(IS).mon(:,1:fs.max_x_incis,index2) = mon;

            list(IS).inn = meet(list(IS).cinn,2);       % compute strip inner approximation
            list(IS).out = meet(list(IS).cout,2);       % compute strip outer approximation

            list(IS).index = [index true(1,nnz(index1))];

            list(IS).ncells = list(IS).ncells + TC;           % number of cells

        elseif wxp_max>param.eps                    % Bisect strip

            %disp('Bisect Strip')

            x = list(IS).x;     
            [L,R] = bis(x(fs.pv(mxp(ixp_max)),1)); % bisect non-monotonic proper intervals

            % Concatenate strips for evaluation (to speed up)
            xaux = x;
            x(fs.pv(mxp(ixp_max)),:) = L; 
            xaux(fs.pv(mxp(ixp_max)),:) = R;
            x = [x xaux];
            [~, TN] = size(x);
            NE = TN/2;      % Number of elements per strip

            [mon] = evaluate_monotony(x,fs.max_x_incis,fs); % evaluate monotony 

            % calculate monotony along the strip
            [N,M,Z] = size(mon);
            monL = (abs(prod(mon(:,:,1:NE),3)) == 1);  
            monL = monL(:,1);
            monL = repmat(monL,1,NE);
            monR = (abs(prod(mon(:,:,NE+1:TN),3)) == 1); 
            monR = monR(:,1);
            monR = repmat(monR,1,NE);
            monpv = [monL monR];
            monpv = reshape(monpv,N,1,Z);

            [inn,out] = obj_func(x,mon,monpv,fs);  % compute inner and outer approximation

            inn = interval(squeeze(inn.lower),squeeze(inn.upper))';
            out = interval(squeeze(out.lower),squeeze(out.upper))';

            % Assign results to the resulting bisected strips
            list(IS).x = x(:,1:NE);
            list(IS).cinn = inn(1:NE);
            list(IS).cout = out(1:NE);
            list(IS).mon = mon(:,:,1:NE); 
            list(IS).inn = meet(inn(1:NE),2);
            list(IS).out = meet(out(1:NE),2);

            LS = numel(list); % number of strips
            list(LS+1).x = x(:,NE+1:TN);
            list(LS+1).cinn = inn(NE+1:TN);
            list(LS+1).cout = out(NE+1:TN);
            list(LS+1).mon = mon(:,:,NE+1:TN); 
            list(LS+1).inn = meet(inn(NE+1:TN),2);       % strip inner approximation
            list(LS+1).out = meet(out(NE+1:TN),2);       % strip outer approximation
            list(LS+1).index = list(IS).index;
            list(LS+1).bisect = true;

            list(LS+1).ncells = list(IS).ncells;
 
        else
            list(IS).index(index1) = false;
        end
    
    end
    
    Inner = join([list(:).inn],2);  % inner approximation
    Outer = join([list(:).out],2);  % outer approximation
    
    TOL = tol(Inner,Outer);
    
    iter = iter + 1;
     
    if mod(iter,param.iter)==0 && param.print
        if fs.dual_flag
          disp(['Iter: ' num2str(iter) [' f*= [[' num2str(Outer.upper) ',' num2str(Outer.lower) '],[' num2str(Inner.upper) ',' num2str(Inner.lower) ']]'] ' Tol: ' num2str(TOL) ' Time: ' num2str(etime)])
        else
          disp(['Iter: ' num2str(iter) [' f*= [[' num2str(Inner.lower) ',' num2str(Inner.upper) '],[' num2str(Outer.lower) ',' num2str(Outer.upper) ']]'] ' Tol: ' num2str(TOL) ' Time: ' num2str(etime)])
        end
    end
  
    etime = cputime-itime;      % cpu time
end

% Apply dual to the solution
if fs.dual_flag
   aux = Inner;
   Inner = dual(Outer);
   Outer = dual(aux); 
end

if param.print
    disp('Finished!')
    disp(' ')
    disp(['Total Elapsed Time(s): ' num2str(etime)])
    disp(['Total Iterations: ' num2str(iter)])
    display_approx(Inner, Outer);       % display approximation
end


end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Evaluate derivatives
function [mon] = evaluate_monotony(x,max_x_incis,fs) 

    x = prop(x);
    
    [dx] = partial_derivatives(x,fs);

    % Find monotony sense
    dx_imon = ge(dx,0); % variables
    dx_dmon = -le(dx,0);
    dx_zmon = eq(dx,0);
    
    dx_mon = dx_imon + dx_dmon + dx_zmon; 
        
    [dxi] = partial_derivatives_incidence(x,max_x_incis,fs);
    
    dxi_imon = ge(dxi,0); % incidences
    dxi_dmon = -le(dxi,0);

    dxi_mon = dxi_imon + dxi_dmon;
    
    [r, c] = size(dxi_mon);
    if ~isempty(dxi_mon)
        nc = numel(dxi_mon(1,1,:));
    else
        nc=0;
    end
    if numel(dx_mon)==r*nc
        dx_mon = reshape(dx_mon,r,1,nc);
    end

    mon = bsxfun(@times,dx_mon,dxi_mon); 
    mon = bsxfun(@times,abs(prod(mon,2)),mon);
    
    %mon = mon*0;
    
end

function display_approx(inn, out)
 
disp(['f*= [[' num2str(inn.lower) ',' num2str(inn.upper) '],[' num2str(out.lower) ',' num2str(out.upper) ']]']) 

disp(['Tolerance= ' num2str(tol(inn,out))])
disp(' ')
disp('-------------------------------------------------------------------')

end

% Stop by inclusion or exclusion
function [stop] = inclusion_exclusion(Inner, Outer,Dual,InnOutV,IncExcV,Interval)

    stop = false;
    if Dual
        Inn = dual(Outer);
        Out = dual(Inner); 
    else
        Inn = Inner;
        Out = Outer; 
    end
    
    for k=1:numel(InnOutV)
        
        InnOut = InnOutV{k};
        IncExc = IncExcV{k};
        
        if strcmp(InnOut,'inn')
            Approx = Inn;
        else
            Approx = Out;
        end
        
        switch IncExc
            case 'inc'        
                 if isine(Approx,Interval)
                    stop = true;
                    break
                  end    
            case 'ninc'
                  if ~isine(Approx,Interval)
                     stop = true;
                     break
                  end
            case 'con'
                  if isine(Interval,Approx)
                     stop = true;
                     break
                  end  
            case 'ncon'
                  if ~isine(Interval,Approx)
                     stop = true;
                     break
                  end
            case 'int'
                  if isint(Approx,Interval)
                     stop = true;
                     break
                  end       
            case 'nint'
                  if ~isint(Approx,Interval)
                     stop = true;
                     break
                  end  
            case 'grt'
                  if ge(Approx,Interval)
                     stop = true;
                     break
                  end 
            case 'ngrt'
                  if ~ge(Approx,Interval)
                     stop = true;
                     break
                  end 
            case 'les'
                  if le(Approx,Interval)
                     stop = true;
                     break
                  end 
            case 'nles'
                  if ~le(Approx,Interval)
                     stop = true;
                     break
                  end 
        end    
    end
    
end

% Onjective function call
function [Inn, Out] = obj_func(ix,mon,mons,fs) 

[i, j, k]=size(mon);
xr = interval([]);
xr(:,1,:)=ix;
xr = repmat(xr,1,j);
x = xr;

% Outer approximation
xp = isprop(xr);
xi = isimpr(xr);
x(xp) = XD(xr(xp),mon(xp));
x(xi) = XDt(xr(xi),mon(xi));

eval(['Out =' fs.f]);

% Inner approximation
x(xi) = dual(XD(xr(xi),mon(xi)));

if ~isempty(fs.pv) 
    mons = reshape(mons,[i,1,k]); 
    mons = bsxfun(@times,mon,mons);
    x(xp) = dual(XDt(xr(xp),mons(xp)));   
end

eval(['Inn =' fs.f]);

Inn = dual(Inn);

end 

% partial derivatives call
function [dx] = partial_derivatives(ix,fs) 

[nv, nc] = size(ix);
dx = interval([]);
dx(1:nv,1:nc) = interval(1);

for k=1:numel(fs.df) 
    eval(['dx(k,:)=' fs.df{k}]);
end

end 

% partial derivatives (incidence) call
function [dx] = partial_derivatives_incidence(ix,ni,fs) 

[nv, nc] = size(ix);
dx = interval([]);
dx(1:nv,1:ni,1:nc) = interval(1);

for i=1:nv 
    for j=1:ni
        if ~isempty(fs.dfi{i,j})
            eval(['dx(i,j,:) =' fs.dfi{i,j}]);
        end
    end
end


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% 
%         if strcmp(InnOut,'inn')
%             if strcmp(IncExc,'inc')   
%                 if isine(Inn,Interval)
%                     stop = true;
%                     break
%                 end  
%             elseif strcmp(IncExc,'ninc')
%                 if ~isine(Inn,Interval)
%                      stop = true;
%                      break
%                 end
%             elseif strcmp(IncExc,'exc')
%                 if isout(Inn,Interval)
%                      stop = true;
%                      break
%                 end
%             elseif strcmp(IncExc,'int')
%                 if isint(Inn,Interval)
%                      stop = true;
%                      break
%                 end
%             end
%         elseif strcmp(InnOut,'out')
%             if strcmp(IncExc,'inc')
%                 if isine(Out,Interval)
%                      stop = true;
%                      break
%                 end    
%             elseif strcmp(IncExc,'ninc')
%                 if ~isine(Out,Interval)
%                      stop = true;
%                      break
%                 end
%             elseif strcmp(IncExc,'exc')
%                 if isout(Out,Interval)
%                      stop = true;
%                      break
%                 end  
%             elseif strcmp(IncExc,'int')
%                 if isint(Out,Interval)
%                      stop = true;
%                      break
%                 end 
%             end
%         end
        
% % Stop by inclusion or exclusion
% function [stop] = inclusion_exclusion(Inner, Outer,Dual,InnOutV,IncExcV,Interval)
% 
%     stop = false;
%     if Dual
%         Inn = dual(Outer);
%         Out = dual(Inner); 
%     else
%         Inn = Inner;
%         Out = Outer; 
%     end
%     
%     for k=1:numel(InnOutV)
%         
%         InnOut = InnOutV{k};
%         IncExc = IncExcV{k};
%         
%         if strcmp(InnOut,'inn')
%             if strcmp(IncExc,'inc')   
%                 if isine(Inn,Interval)
%                     stop = true;
%                     break
%                 end  
%             elseif strcmp(IncExc,'ninc')
%                 if ~isine(Inn,Interval)
%                      stop = true;
%                      break
%                 end
%             end
%         elseif strcmp(InnOut,'out')
%             if strcmp(IncExc,'inc')
%                 if isine(Out,Interval)
%                      stop = true;
%                      break
%                 end    
%             elseif strcmp(IncExc,'ninc')
%                 if ~isine(Out,Interval)
%                      stop = true;
%                      break
%                 end
%             end
%         end  
%     end
%     
% end
