%% MIC V1.0
% Authors: Pau Herrero (1) and M.A. Sainz (2)
% Coded by Pau Herrero
% (1) Center for Bio-Inspired Technology, Department of Electrical and
% Electronic Engineering Imperial College London. pherrero@imperial.ac.uk
% (2) Institut d'Insformarica i Aplicacions, Universitat de Girona
% 07/05/2015
%
% GNU General Public License:
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  function [inn,out] = fstar(vars,f,param)
  
  % Default parameters if they do not exist
    if ~isfield(param,{'fstar'})
        param.fstar = true;
    end
  
    if param.fstar
        [x, fs] = fstar_symbolic(f,vars,param);  % Symbolic routine

        [inn,out] = fstar_numeric(x,fs,param);  % Numeric routine
    else
            
        for k=1:numel(vars)
           eval(sprintf('%s=%s',vars{k}{1},['interval([' num2str(vars{k}{2}(1),'%10.20e\n') ','  num2str(vars{k}{2}(2),'%10.20e\n') ']);' ])); 
        end
        inn = [];
        out = eval(f);
    
    end
    
  end

