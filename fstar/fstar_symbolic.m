%% MIC V1.0
% Authors: Pau Herrero (1) and M.A. Sainz (2)
% Coded by Pau Herrero
% (1) Center for Bio-Inspired Technology, Department of Electrical and
% Electronic Engineering Imperial College London. pherrero@imperial.ac.uk
% (2) Institut d'Insformatica i Aplicacions, Universitat de Girona
% 07/05/2015
%
% GNU General Public License:
%     This program is free software: you can redistribute it and/or modify
%     it under the terms of the GNU General Public License as published by
%     the Free Software Foundation, either version 3 of the License, or
%     (at your option) any later version.
% 
%     This program is distributed in the hope that it will be useful,
%     but WITHOUT ANY WARRANTY; without even the implied warranty of
%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%     GNU General Public License for more details.
% 
%     You should have received a copy of the GNU General Public License
%     along with this program.  If not, see <http://www.gnu.org/licenses/>.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Symbolic part

function [x, fs] = fstar_symbolic(f,var,param)

% Default parameters if they do not exist
if ~isfield(param,{'simpl'})
    param.simpl = 0;
end


%% Symbolic simplification of the objective function
if param.simpl
        
    % Check if there is any pre-defined monotonous variable
    simpl = 1;
    for i=1:numel(var)
        if numel(var{i})==3 && char(var{i}(3))=='m'
            simpl = 0;
            break;
        end
    end
    
    if simpl  
        for i=1:numel(var)
            syms(char(var{i}(1))); 
        end
        ninc = num_incidences(f,var);
        f1 = char(simplify(eval(f)));  % simplify epression
        ninc1 = num_incidences(f1,var);
        f2 = char(collect(eval(f)));   % collect simplify epression
        ninc2 = num_incidences(f2,var);
        if ninc1 < ninc && ninc && ninc2 % select function wiht les incidences 
            f = f1;
        elseif ninc2 < ninc && ninc2 < ninc1
            f = f2;
        end
    end
    
end

f = [ '(' f ')' ';'];   % Add a semi-colon

%% Evaluate global tree-optimality
%fs.tree_opt = tree_optimality(f);      % needs revision
fs.tree_opt = 0;


%% Eliminate unnecessary variables from the variable vector
for i=1:numel(var)
  str = char(var{i}(1)); 
  vars{i} = str;
end 

index = [];
for k=1:numel(vars)
    expression = ['\W*' vars{k} '\W*'];
    startIndex = regexp(f,expression);
    if isempty(startIndex)
        index = [index k];
    end
end
var(index) = [];

%% Extract variables and intervals from cell struct
x = interval([]);
vars = [];
fs.mvars = [];
n=1;
m=1;
for i=1:numel(var)
    if numel(var{i})==3 && char(var{i}(3))=='m' % monotonic variables that do not need to be bisected
        fs.mvars(n) = i;    % save index of monotonic variables
        n=n+1;
    end
    str = char(var{i}(1));          % variables that potentially need to be bisected
    vars{m} = str;
    intv = var{i}(2);
    x(m,:) = intv{1};  % interval vector
    m=m+1;
end

%% Find multi incident varibles
f = strtrim(f);
f_inc = f;
f_inc(f_inc==' ')=[]; % remove white spaces
f_inc = [ f_inc ';'];   % Add a semi-colon
fs.num_incis = [];
incis_vec = [];
fs.max_x_incis = 0;   % maximum value of proper variable incidences
for v=1:numel(vars)
    incis = [];
    expression1 = [ '\W' vars{v}];     % Use regular expression
    expression2 = [vars{v} '\W'];     % Use regular expression
    [index1,endIndex1] = regexp(f_inc,expression1);
    [index2,endIndex2] = regexp(f_inc,expression2);  
    index = intersect(index1,index2-1);
    endIndex = intersect(endIndex1+1,endIndex2);
    
    k=1;
    while ~isempty(index)
       f_inc = [f_inc(1:endIndex(1)-1) '_' num2str(k) f_inc(endIndex(1):end)];
       incis = cellstr([incis; vars{v} '_' num2str(k)]);
       index(1) = [];
       index = index + 1 + numel(num2str(k)); 
       endIndex(1) = [];
       endIndex = endIndex + 1 + numel(num2str(k));
       fs.max_x_incis = max(fs.max_x_incis,k);
       k = k+1;
    end
    
    
    if ~isempty(incis)
        incis_vec{v} =  cellstr(incis);
        fs.num_incis(v) = numel(incis_vec{v}); % number of incidences   
    end
end

%% generate function and partial derivatives
[fs.f] = generate_function_file(f_inc, x, incis_vec, fs.num_incis,fs,param);
[fs.df, fs.dfi] = generate_partial_derivatives_files(f,f_inc, vars, incis_vec,fs,param);


end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% count number of incidences
function [ninc] = num_incidences(f,var)
    ninc = 0;
    for i=1:numel(var)
        k = strfind(f, char(var{i}(1)));
        ninc = ninc + numel(k);
    end
end

function [f] = generate_function_file(f_inc, x, incis_vec,num_incis,fs,param)

% Generate function file
% replace incidences
f_aux = f_inc;
for j=1:numel(incis_vec)
    for i=numel(incis_vec{j}):-1:1
         expression1 = ['\W' char(incis_vec{j}(i))];      
         expression2 = [char(incis_vec{j}(i)) '\W']; 
         [iex1, eex1] = regexp(f_aux,expression1);
         [iex2, eex2] = regexp(f_aux,expression2);
         iex = intersect(iex1,iex2-1);
         eex = intersect(eex1+1,eex2);
         new_exp = ['x(' num2str(j) ',' num2str(i) ',:)'];
         for k=1:numel(iex)
           f_aux = [f_aux(1:iex(k)) new_exp f_aux(eex(k):end)];
         end
    end
end

f = f_aux;

end


function [df_var, df_inc] = generate_partial_derivatives_files(f,f_inc, vars, incis_vec,fs,param)

%% Calculate partial derivatives with respect to variables

% eliminate duals from expression
expression = 'dual';
replace = '';
f = regexprep(f,expression,replace);
f_inc = regexprep(f_inc,expression,replace);

for i=1:numel(vars)
   syms(vars{i}); 
end
f_sym = eval(f);
 
df_sym = [];
df_var = [];

for i=1:numel(vars)
    
    if isempty(find(fs.mvars==i)) % check if the variable is predefined as monotonous (i.e. 'm')
    
        vars_sym(i) = sym(vars{i});
        df_sym{i} = diff(f_sym,vars_sym(i));
        df_var{i} = strtrim(char(df_sym{i}));

        if param.simpl  % Simplify expression
            df = char(df_sym{i});
            ninc = num_incidences(df,vars);
            df1 = char(simplify(sym(df_sym{i})));  % simplify epression
            ninc1 = num_incidences(df1,vars);
            df2 = char(collect(sym(df_sym{i})));   % collect simplify epression
            ninc2 = num_incidences(df2,vars);
            if ninc1 < ninc && ninc && ninc2 % select function with les incidences 
                df = [df1];
            elseif ninc2 < ninc && ninc2 < ninc1
                df = [df2];
            end
            df_var{i} = strtrim(df); % eliminate white spaces
        end
        
    else 
        df_var{i} = '1'; % set to monotonous
    end
   
end

%% Generate partial derivatives file

for i=1:numel(df_var)
    f_aux = df_var{i};
    f_aux(f_aux==' ')=[]; % remove white spaces
    f_aux = ['(' f_aux ');'];
    for j=1:numel(vars) 
        expression1 = ['\W' vars{j}];      
        expression2 = [vars{j} '\W']; 
        [iex1, eex1] = regexp(f_aux,expression1);
        [iex2, eex2] = regexp(f_aux,expression2);
        iex = intersect(iex1,iex2-1);
        eex = intersect(eex1+1,eex2);
         
        new_exp = ['ix(' num2str(j) ',:)'];
        numchar = numel(new_exp) - numel(vars{j});

      while ~isempty(iex)
           f_aux = [f_aux(1:iex(1)) new_exp f_aux(eex(1):end)];
           iex(1) = [];
           iex = iex + numchar; 
           eex(1) = [];
           eex = eex +  numchar; 
        end 
        
    end
 
    df_var{i} = f_aux;
end 

%% Calculate symbolic partial derivatives with respect to incidence

for j=1:numel(incis_vec)
    for i=1:numel(incis_vec{j})
        syms(incis_vec{j}(i)); 
    end
end
f_inc_sym = eval(f_inc);

for j=1:numel(incis_vec)
    for i=1:numel(incis_vec{j})
        if isempty(find(fs.mvars==j)) % check if the variable is predefined as monotonous  (i.e. 'm')
            incis_sym = sym(incis_vec{j}(i));
            df_inc_sym{j,i} = diff(f_inc_sym,incis_sym); 
            exp = char(df_inc_sym{j,i});
            df_inc{j,i} = [ '(' exp ');'];
        else
           df_inc{j,i} = '1;';  % set to monotonous
        end
    end
end

% eliminate index of multi-incides
for j=1:numel(incis_vec)
    for i=1:numel(incis_vec{j})
        exp = df_inc{j,i};
        expression = '_(\d)+';     % Use regular expression
        df_inc{j,i}  = regexprep(exp,expression,'');          
    end
end

if param.simpl  % simplify expression
    for j=1:numel(incis_vec)
        for i=1:numel(incis_vec{j})
            ninc = num_incidences(df_inc{j,i},incis_vec);
            ddf1 = char(simplify(sym(eval(df_inc{j,i}))));  % simplify epression
            ninc1 = num_incidences(ddf1,incis_vec);
            ddf2 = char(collect(sym(eval(df_inc{j,i}))));   % collect simplify epression
            ninc2 = num_incidences(ddf2,incis_vec);
            if ninc1 < ninc && ninc && ninc2 % select function wiht les incidences 
                df_inc{j,i} = [ddf1 ';'];
            elseif ninc2 < ninc && ninc2 < ninc1
                df_inc{j,i} = [ddf2 ';'];
            end 
        end
    end
end

for j=1:numel(incis_vec)
    for i=1:numel(incis_vec{j})
 
        f_aux = df_inc{j,i};
        f_aux(f_aux==' ')=[]; % remove white spaces
        for k=1:numel(vars)
            
            expression1 = ['\W' vars{k} ];      
            expression2 = [vars{k} '\W']; 
            [iex1, eex1] = regexp(f_aux,expression1);
            [iex2, eex2] = regexp(f_aux,expression2);
            iex = intersect(iex1,iex2-1);
            eex = intersect(eex1+1,eex2);
            
            new_exp = ['ix(' num2str(k) ',:)'];
            numchar = numel(new_exp) - numel(vars{k});
            
            while ~isempty(iex)
               f_aux = [f_aux(1:iex(1)) new_exp f_aux(eex(1):end)];
               iex(1) = [];
               iex = iex + numchar; 
               eex(1) = [];
               eex = eex +  numchar; 
            end 
        end
         
        df_inc{j,i} = f_aux;
    end
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% function tree_opt = tree_optimality(f)
% 
% expression = [vars{k} '([+*/^)-]|;)']; 
% [iex, eex] = regexp(f,expression);
% 
% 
% end


% function tree_opt = tree_optimality(f)
% % evaluate tree optimality
% tree_opt = 1;   % tree optimality flag
% Index  = regexp(f,'[*/^]'); % get index for the  non-monotonic binary operator (* or /)
% 
% % remove indices that the operator has a number as an operant
% Indices = [];
% lnumeric = 1;
% rnumeric = 1;
% for i=1:numel(Index)        % loop over the  non-monotonic binary operator indices
%     
%     if  f(Index(i)) == '*' || f(Index(i)) == '/'
%     left = f(1:Index(i)-1); % expression on the left side of the non-monotonic binary operator (* or /)
%     right = f(Index+1:end); % expression on the right side of the non-monotonic binary operator (* or /)
% 
%     for j=numel(left):-1:1
%         aux = str2num(left(j));
%         if ~isempty(aux) || left(j) == '.'
%         elseif left(j) == '+' || left(j) == '-'
%             break
%         else
%             lnumeric = 0;
%             break
%         end
%     end
%     
%     for j=1:numel(right)
%         aux = str2num(right(j));
%         if ~isempty(aux) || right(j) == '.'
%         elseif right(j) == '+' || right(j) == '-'
%             break
%         else
%             rnumeric = 0;
%             break
%         end
%     end
%     else
%        lnumeric = 0;
%        rnumeric = 0;
%     end
%     
%     
%     if lnumeric == 0 && rnumeric == 0      
%         Indices = [Indices Index(i)]; 
%     else
%         f(Index(i)) = '#';  % replace non-monotonic operant by #
%     end
%     
% end
% Index = Indices;
% 
% for i=1:numel(Index)        % loop over the  non-monotonic binary operator indices
%     
%     left = f(1:Index(i)-1); % expression on the left side of the non-monotonic binary operator (* or /)
%     right = f(Index+1:end); % expression on the right side of the non-monotonic binary operator (* or /)
%     
%     % evaluate expression on the left side
%     rbracket = 0; % right bracket flag
%     lbracket = 0; % left bracket flag
%     for j=numel(left):-1:1
%         if left(j) == ')'
%            rbracket = 1; 
%         elseif left(j) == '('
%            lbracket = 1; 
%         elseif left(j) == '+' || left(j) == '-' 
%             if rbracket == 1 && lbracket == 0 
%                 tree_opt = 0;
%                 break
%             else
%                 break
%             end
%         elseif left(j) == '*' || left(j) == '/' || left(j) == '^'
%             tree_opt = 0;
%             break
%         end
%     end
%     
%     % evaluate expression on the right side
%     rbracket = 0; % right bracket flag
%     lbracket = 0;  % left bracket flag
%     for j=1:numel(right)
%         if right(j) == '('
%            lbracket = 1; 
%         elseif right(j) == ')'
%            rbracket = 1; 
%         elseif right(j) == '+' || right(j) == '-'
%             if lbracket == 1 && rbracket == 0 
%                 tree_opt = 0;
%                 break
%             else
%                 break
%             end
%         elseif right(j) == '*' || right(j) == '/' || right(j) == '^'
%             tree_opt = 0;
%             break
%             
%             
%         end
%     end
%     if tree_opt == 0
%         break
%     end
% end
% 
% end
%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
