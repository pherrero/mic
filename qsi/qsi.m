function [S,U,N] = qsi(C,VARS,param,EPS,parallel)

qsi.eps = EPS;

%% Symbolic QSI

% Extract constraints
for k=1:numel(C) 
    %f = [C{k}{1} ';'];
    f = [C{k}{1}];
    qsi.f{k} = f;
    qsi.intv{k} = C{k}{2};  
end

% Extract variables
qsi.fvars = [];
qsi.fintv = interval([]);
qsi.fmonv = {}; 
qsi.uvars = [];
qsi.uintv = interval([]);
qsi.umonv = {};
qsi.evars = [];
qsi.eintv = interval([]);
qsi.emonv = {};

for k=1:numel(VARS) 
    var = VARS{k}{1};
    intv = interval([VARS{k}{2}]);
    Q = VARS{k}{3};
    
    if numel(VARS{k})==4 && ~isempty(VARS{k}{4}) % monotony vector
        monv = VARS{k}{4};
    else
        monv = [];
    end 
    
    if Q == 'F'
        qsi.fvars{numel(qsi.fvars)+1} = var;
        qsi.fintv(numel(qsi.fvars),:) = intv;
        qsi.fmonv{numel(qsi.fvars)} = monv;
    elseif Q == 'U'
        qsi.uvars{numel(qsi.uvars)+1} = var;
        qsi.uintv(numel(qsi.uvars),:) = intv;
        qsi.umonv{numel(qsi.uvars)} = monv;
    elseif Q == 'E'
        qsi.evars{numel(qsi.evars)+1} = var;
        qsi.eintv(numel(qsi.evars),:) = dual(intv); 
        qsi.emonv{numel(qsi.evars)} = monv;
    end
end


% Build fstar problems

for k=1:numel(qsi.f)
   
    vars=[];
    ifvars = [];     % index of free variables (initialisation)
    
    % Look for free variables
    for i=1:numel(qsi.fvars)
        I = strfind(qsi.f{k},qsi.fvars{i});
        if ~isempty(I)
            if ~isempty(qsi.fmonv{i})
                vars{numel(vars)+1} = {qsi.fvars{i},[qsi.fintv(i).lower,qsi.fintv(i).upper],qsi.fmonv{i}{k}};
            else
                vars{numel(vars)+1} = {qsi.fvars{i},[qsi.fintv(i).lower,qsi.fintv(i).upper]};
            end         
            ifvars(numel(ifvars)+1) = i;
        end    
    end
    
    % Look for universal variables
    for i=1:numel(qsi.uvars)
        I = strfind(qsi.f{k},qsi.uvars{i});
        if ~isempty(I)
           if ~isempty(qsi.umonv{i})
               vars{numel(vars)+1} = {qsi.uvars{i},[qsi.uintv(i).lower,qsi.uintv(i).upper],qsi.umonv{i}{k}};
            else
               vars{numel(vars)+1} = {qsi.uvars{i},[qsi.uintv(i).lower,qsi.uintv(i).upper]}; 
            end
        end    
    end
    
    % Look for existensial variables
    for i=1:numel(qsi.evars)
        I = strfind(qsi.f{k},qsi.evars{i});
        if ~isempty(I)
            if ~isempty(qsi.emonv{i})
                vars{numel(vars)+1} = {qsi.evars{i},[qsi.eintv(i).lower,qsi.eintv(i).upper],qsi.emonv{i}{k}};
            else
                vars{numel(vars)+1} = {qsi.evars{i},[qsi.eintv(i).lower,qsi.eintv(i).upper]};
            end
            
            %qsi.sevars(k,i) = true; % existencial variables matrix
            
        end    
    end
    
    [x, fs] = fstar_symbolic(qsi.f{k},vars,param);
    
    qsi.x{k} = x;
    qsi.fs{k} = fs;
    qsi.ifvars{k} = ifvars;
    
end

%qsi.ssevars = sum(qsi.sevars,2) > 1; % sum shared existencial variables

%% Numeric QSI

% paralell loop or not
if ~parallel
  parforArg = 0;
else
  parforArg = Inf;
end

disp(' ')
disp(['Running QSI...'])

X = qsi.fintv;  % free variables interval box

L = [X];  % List of intervals
L1 = L;
S = []; % Solution baxes
N = []; % Non-splution
U = []; % Undefined
W0 = width(X);  % initial weight

while ~isempty(L1)   % loop until the list is empty   
    
    L = L1;
    L1 = [];
    [~, nboxes] = size(L);
    %disp(['Boxes to evaluate:' num2str(nboxes)])
    parfor(index=1:nboxes,parforArg)  % parallel loop until the list is empty 
    %for index=1:nboxes  % loop until the list is empty 
        
       X = L(:,index);        % get first box of the list

       consist = consistency_test(X, qsi,param);     % test consistency

       if strcmp(consist,'true')              % if consistency is true add box to the solution list
           S = [S X];
%            draw_boxes(X,[],[],1,2) ;          % plot boxes 
%            pause(0.01)
        elseif strcmp(consist,'false')         % if consistency is false add box to the non-solution list
           N = [N X];
%            draw_boxes([],[],X,1,2) ;          % plot boxes 
%            pause(0.01)
       else                                    % otherwise box is undefined
           %W = width(X);                       % compute width
           W = width(X)./W0;                  % compute relative width

           [maxW, i_max] = max(W);           % get index of the widest dimention

           if maxW > qsi.eps                 % if maximum with is bigger than epsilon 
                X1 = X;
                X2 = X;
                [X1(i_max),X2(i_max)] = bis(X(i_max));  % bisect the widest dimention
                L1 = [L1 X1];
                L1 = [L1 X2];
           else
                U = [U X];                     % add undefined box to the list (width < epsilon)         
           end
       end

    end
end

end


% Consistecy function
function C = consistency_test(X, qsi,param)

T = [];
F = [];
U = [];

[r,c]=size(X);

for k=1:numel(qsi.f)        % iterate along constrants

    x = qsi.x{k};           % free variables vector
    fs = qsi.fs{k};         % fstar structure
    ifvars = qsi.ifvars{k}; % index of the free variables in 
    intv = qsi.intv{k};     
    
    x(ifvars) = dual(X(ifvars));  
    param.inex = {{'inn','out'},{'ninc','inc'},intv};
    [Inn,Out] = fstar_numeric(x, fs, param);
    if ~isine(Inn,interval(intv))
        F = [F true];
        break
    elseif isempty(U)         % do not need to evaluate if one constrained is already undefined 
        x(ifvars) = X(ifvars);
        param.inex = {{'out','inn'}',{'inc','ninc'},intv};
        [Inn,Out] = fstar_numeric(x, fs, param);

        if isine(Out,interval(intv))
                T = [T true];  
        else
                U = [U true];  
        end
    end
    
        
end

if nnz(T) == numel(qsi.f)
    C = 'true';
elseif nnz(F)>0
    C = 'false';
else
    C = 'undefined';
end

str=[];
for i=1:r
    str = [str '[' num2str(X(i).lower) ',' num2str(X(i).upper) '];']; 
end
disp(['(' str ')  ' C])

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
