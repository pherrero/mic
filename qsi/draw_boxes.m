function draw_boxes(S, E, N, varargin)

    figure ;
    hold on ;

    switch numel(varargin)
        
        
         case 1
            
            x = varargin{1} ;
            
            
            if ~isempty(S) 
                [r, c] = size(S(:,x));
                ONES = repmat(interval([0,1]),r,c);
                draw(S(:,x), ONES, 'r') ; 
            end
            
            if ~isempty(E) 
                [r, c] = size(E(:,x));
                ONES = repmat(interval([0,1]),r,c);
                draw(E(:,x), ONES, 'y') ; 
            end
            if ~isempty(N) 
                [r, c] = size(N(:,x));
                ONES = repmat(interval([0,1]),r,c);
                draw(N(:,x), ONES, 'b') ; 
            end
        
        case 2
            
            x = varargin{1} ;
            y = varargin{2} ;
            
                for k = 1:size(x,2)

                    if ~isempty(S) draw(S(:,x(1,k)), S(:,y(1,k)), 'r') ; end
                    if ~isempty(E) draw(E(:,x(1,k)), E(:,y(1,k)), 'y') ; end
                    if ~isempty(N) draw(N(:,x(1,k)), N(:,y(1,k)), 'b') ; end

                end
            
        case 3
            
                fac = [1 2 3 4; 4 3 5 6; 6 7 8 5; 1 2 8 7; 6 7 1 4; 2 3 5 8];
                if ~isempty(S) 

                  for k=1:numel(S(:,1).lower)  
                      xl = S(k,1).lower;
                      xh = S(k,1).upper;
                      yl = S(k,2).lower;
                      yh = S(k,2).upper;
                      zl = S(k,3).lower;
                      zh = S(k,3).upper;
                      vert = [xh yh zl;  xl yh zl;  xl yh zh;  xh yh zh;  xl yl zh; xh yl zh;  xh yl zl; xl yl zl];
                      patch('Faces',fac,'Vertices',vert,'FaceColor','r');  % patch function
                  end
                  
                end
                if ~isempty(E) 
                  for k=1:numel(E(:,1).lower) 
                      xl = E(k,1).lower;
                      xh = E(k,1).upper;
                      yl = E(k,2).lower;
                      yh = E(k,2).upper;
                      zl = E(k,3).lower;
                      zh = E(k,3).upper;
                      vert = [xh yh zl;  xl yh zl;  xl yh zh;  xh yh zh;  xl yl zh; xh yl zh;  xh yl zl; xl yl zl];
                      patch('Faces',fac,'Vertices',vert,'FaceColor','y');  % patch function
                  end
                    end
                if ~isempty(N) 
                  for k=1:numel(N(:,1).lower)   
                      xl = N(k,1).lower;
                      xh = N(k,1).upper;
                      yl = N(k,2).lower;
                      yh = N(k,2).upper;
                      zl = N(k,3).lower;
                      zh = N(k,3).upper;
                      vert = [xh yh zl;  xl yh zl;  xl yh zh;  xh yh zh;  xl yl zh; xh yl zh;  xh yl zl; xl yl zl];
                      patch('Faces',fac,'Vertices',vert,'FaceColor','b');  % patch function
                  end
                end

                material metal;
                alpha('color');
                alphamap('rampdown');
                view(3);
            
%             x = varargin{1} ;
%             y = varargin{2} ;
%             z = varargin{3} ;
%             
%             for k = 1:size(x,2)
% 
%                 if ~isempty(S) 
%                     S2 = mid(S); 
%                     plot3(S2(:,x(1,k)),S2(:,y(1,k)),S2(:,z(1,k)),'o','MarkerSize',4,'MarkerEdgeColor',[.25 0 0],'MarkerFaceColor',[1 0 0]) ;
%                 end
%                 if ~isempty(E) 
%                     E2 = mid(E) ;    
%                     plot3(E2(:,x(1,k)),E2(:,y(1,k)),E2(:,z(1,k)),'o','MarkerSize',1,'MarkerEdgeColor',[.25 .25 0],'MarkerFaceColor',[1 1 0]) ;
%                 end
%                 if ~isempty(N) 
%                     N2 = mid(N) ;
%                     plot3(N2(:,x(1,k)),N2(:,y(1,k)),N2(:,z(1,k)),'o','MarkerSize',3,'MarkerEdgeColor','k','MarkerFaceColor',[0 0 1]) ;
%                 end
%             end
            
    end

end
