function example_min()

    % Variables definition
    VARS{1} = {'x1',[-10,10],'F'};  % Free variable
    VARS{2} = {'x2',[-10,10],'F'};  % Free variable
    VARS{3} = {'v',[-1,1],'E'}; % Universally quantified variable

    % Constraint definition
    C{1} = {'min(-x1+x2*v+x1^2,-x2+(1+x1^2)*v+v^3)',[0,inf]};  % f \in [0 0]

    % QSI epsilon    
    eps = 0.05;

    % fstar parameters
    param.tol = 0.1;   % Tolerance
    param.eps = 0.1;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = true;

    tic
    [S,U,N] = qsi(C,VARS,param,eps,false);    % call qsi
    toc
    
    % S: Array of solution boxes
    % N: Non-solution boxes
    % U: Undefined boxes
    
    % X1 = S(:,1) % To extract first interval box of S
    % I1 = X1(1) % To extract first interval of the interval box X1
    
    draw_boxes(S',U',N',1,2) ;          % plot boxes (red:solution; blue: non-solution; white: undefined)
    
return