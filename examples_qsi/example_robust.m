function example_robust()

    % free variables
    VARS{1} = {'c1',[0.1,1],'F'};
    VARS{2} = {'c2',[0.1,1],'F'};
    
    % Quantified variables
    VARS{3} = {'p1',[0.9,1.1],'U'};
    VARS{4} = {'p2',[0.9,1.1],'U'};
    VARS{5} = {'p3',[0.9,1.1],'U'};
    
    % Constraints
    C{1} = {'c1*p1*p3^2',[0,inf]};
    C{2} = {'(p2*p3+1)^2 - p2*(p3+c2*p1*p3)',[0,inf]};                                              % constraint  f \in [0 inf]
    C{3} = {'(1+c2*p1)*((p2*p3^2+p3)*(p2*p3+1)-p2*(p3^2+c2*p1*p3^2))-(p2*p3+1)^2*(c1*p1)',[0,inf]}; % constraint  f \in [0 inf]

    % QSI epsilon    
    eps = 0.02;

    % fstar parameters
    param.tol = 0.1;   % Tolerance
    param.eps = 0.1;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = false;

    tic
    [S,U,N] = qsi(C,VARS,param,eps,true);    % call qsi
    toc
    
    draw_boxes(S',U',N',1,2) ;          % plot boxes 

return


