function example_simple()

    % Variables definition
    VARS{1} = {'ICR',[2,6],'F'};  % Free variable
    VARS{2} = {'ISF',[0,100],'F'};  % Free variable
    VARS{3} = {'CHO1',[40,50],'E'}; % Existentially quantified variable
    VARS{4} = {'G1',[140,150],'E'}; % Existentially quantified variable
    VARS{5} = {'IOB1',[0.5,1],'E'}; % Existentially quantified variable
    VARS{6} = {'CHO2',[70,80],'E'}; % Existentially quantified variable
    VARS{7} = {'G2',[160,170],'E'}; % Existentially quantified variable
    VARS{8} = {'IOB2',[2,2.5],'E'}; % Existentially quantified variable
    VARS{9} = {'Gmin2',[140,150],'E'}; % Existentially quantified variable

    
    % Constraint definition
    C{1} = {'CHO1/ICR+(G1-120)/ISF-IOB1',[10,10.2]};  % f \in [0 0]
    C{2} = {'CHO2/ICR+((G2-120)-(Gmin2-130))/ISF-IOB2',[18,18.5]};  % f \in [0 0]

    % QSI epsilon    
    eps = 0.05;

    % fstar parameters
    param.tol = 0.1;   % Tolerance
    param.eps = 0.1;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = true;

    tic
    [S,U,N] = qsi(C,VARS,param,eps,true);    % call qsi
    toc
    
    % S: Array of solution boxes
    % N: Non-solution boxes
    % U: Undefined boxes
        
    draw_boxes(S',U',N',1,2) ;          % plot boxes (red:solution; blue: non-solution; white: undefined)
    
return