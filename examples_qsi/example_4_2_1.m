
function example_4_2_1()

    VARS{1} = {'x1',[-10,10],'F'};  % free variable
    VARS{2} = {'x2',[-10,10],'F'};  % free variable
    VARS{3} = {'u',[-1,1],'U'};     % universal varible
    VARS{4} = {'v',[-2,2],'E'};     % existential variable

    C{1} = {'x1*u-x2*v^2*sin(x1)',[0,inf]};     % constraint  f \in [0 inf]

    % QSI epsilon    
    eps = 0.05;

    % fstar parameters
    param.tol = 0.1;   % Tolerance
    param.eps = 0.1;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = false;

    tic
    [S,U,N] = qsi(C,VARS,param,eps,true);    % call qsi
    toc
    
    draw_boxes(S',U',N',1,2) ;          % plot boxes 

return
