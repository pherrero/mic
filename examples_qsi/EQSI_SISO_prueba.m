

function EQSI_SISO_prueba()
  clc
  tic
   param.tol = 0.0001;   % Tolerance
   param.eps = 0.0001;   % Epsilon
   param.print = false;
   param.time = 100;    % limit computation time of fstar
   %param.simpl = true;
   param.simpl = false;
   param.iter = 30;
   eps = 0.02;     % epsilon main QSI
   parallel=true

 %Variables
   VARS = [];   
   VARS{numel(VARS)+1} = {'x1',[20,40],'F'};
   VARS{numel(VARS)+1} = {'x2',[3,6],'F'};

   VARS{numel(VARS)+1} = {'q1',[0.4 0.54],'U'}; %|
   VARS{numel(VARS)+1} = {'q2',[0.5 0.54],'U'}; %| Escenario Sc3
   VARS{numel(VARS)+1} = {'q3',[0.5 0.54],'U'}; %|

   VARS{numel(VARS)+1} = {'c',[0.5,2],'E'};
%    VARS{numel(VARS)+1} = {'v',[0.5,1],'E'};
   VARS{numel(VARS)+1} = {'v',[0.5,3],'E'};
   
   % Funciones
   C = [];
   C{numel(C)+1} =  {'c^2-(q1+q2+q3)*(q1+q3-x2)+q1^2*q2^2*q3-q2*q3*x1',[0,0]};  %f1
   %C{numel(C)+1} = {'2*v*c-x2+2*q1+q2+2*q3+0*x1',[0,0]};  %f2
   C{numel(C)+1} =  {'power((x2-2*q1-q2-2*q3)/(2*v),2)-(q1+q2+q3)*(q1+q3-x2)+power(q1,2)*power(q2,2)*q3-q2*q3*x1',[0,0]};    %f2 eliminando  c

   %eps = 0.02;     % epsilon main QSI
   eps = 0.05;     % epsilon main QSI
   parallel = true;

   eps2 = 0.02;     % epsilon main QSI
   e1 = 0.0;      % tolerance InsideQSI of QSI1
   e2 = 0.0;      % tolerance InsideQSI of QSI2

   c_f1 = 6;   %�ndice de la existencial compartida c dentro del vector de
               %variables (x,y,p,c,v) para la primera funci�n. Se sigue el
               %orden de VARS obviando las variables que no aparecen en la
               %funci�n
   c_f2 = 6;   %�ndice de la existencial compartida c dentro el vector de
               %variables (x,y,p,c,v) para la segunda funci�n. Se sigue el
               %orden de VARS obviando las variables que no aparecen en la
               %funci�n
   % fstar parameters
   tic
   %[S,U,N] = qsi_vect(C,VARS,param,eps,parallel,eps2,e1,e2,c_f1,c_f2);  %QSI doble sin eliminar c
   [S,U,N] = qsi(C,VARS,param,eps,parallel); %QSI simple eliminando c

   S
   [fs,cs]=size(S)
   U;
   [fu,cu]=size(U)
   N;
   [fn,cn]=size(N)
   toc
   draw_boxes(S',U',N',1,2); %boxes 2-D, true: red; undefined: yellow;   non-solution: blue 
   
  toc
return

