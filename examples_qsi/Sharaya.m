  %Ejemplo 12 de "IntLinInc3D package. User manual" Irene A. Sharaya
   %eps = 0.05;
   eps = 0.02;

   VARS{1} = {'x1',[-2.5,2.5],'F'};
   VARS{2} = {'x2',[-2.5,2.5],'F'};
   VARS{3} = {'x3',[-2.5,2.5],'F'};
   VARS{4} = {'a11',[-1,1],'E'};
   VARS{5} = {'a12',[-2,2],'E'};
   VARS{6} = {'a13',[-2,2],'E'};
   VARS{7} = {'a21',[-2,2],'E'};
   VARS{8} = {'a22',[-1,1],'E'};
   VARS{9} = {'a23',[-2,2],'E'};
   VARS{10} = {'a31',[-2,2],'E'};
   VARS{11} = {'a32',[-2,2],'E'};
   VARS{12} = {'a33',[-1,1],'E'};
   VARS{13} = {'b1',[2,2],'E'};
   VARS{14} = {'b2',[2,2],'E'};
   VARS{15} = {'b3',[2,2],'E'};

   C{1} = {'a11*x1+a12*x2+a13*x3-b1',[0,0]};  % f \in [0 0]
   C{2} = {'a21*x1+a22*x2+a33*x3-b2',[0,0]};  % f \in [0 0]
   C{3} = {'a31*x1+a32*x2+a33*x3-b3',[0,0]};  % f \in [0 0]

       % fstar parameters
    param.tol = 0.01;   % Tolerance
    param.eps = 0.01;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = false;

    parallel = true;
    
   tic
   [S,U,N] = qsi(C,VARS,param,eps,parallel);    % S: solution N: non-solution; U: undefined
   toc
    
   draw_boxes([],U',[],1,2,3);