function example_simple()

    % Variables definition
    VARS{1} = {'x1',[-2,2],'F'};  % Free variable
    VARS{2} = {'x2',[-2,2],'F'};  % Free variable
    %VARS{3} = {'x3',[-1,1],'E'};  % Existentially quantified variable
    VARS{3} = {'x3',[-1,1],'U'}; % Universally quantified variable

    % Constraint definition
    C{1} = {'x1^2-x2^2+x3',[0,10]};  % f \in [0 0]

    % QSI epsilon    
    eps = 0.05;

    % fstar parameters
    param.tol = 0.1;   % Tolerance
    param.eps = 0.1;   % Epsilon
    param.print = false;
    param.time = 5;    % limit computation time of fstar
    param.simpl = true;

    tic
    [S,U,N] = qsi(C,VARS,param,eps,true);    % call qsi
    toc
    
    % S: Array of solution boxes
    % N: Non-solution boxes
    % U: Undefined boxes
    
    % X1 = S(:,1) % To extract first interval box of S
    % I1 = X1(1) % To extract first interval of the interval box X1
    
    draw_boxes(S',U',N',1,2) ;          % plot boxes (red:solution; blue: non-solution; white: undefined)
    
return