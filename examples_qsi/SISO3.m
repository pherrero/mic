function SISO3()
clc
tic
   VARS{1} = {'x',[-10,10],'F'};  % Free variable
   VARS{2} = {'y',[0,4],'U'};  % Free variable

   % Constraint definition
   C{1} = {'x-y+1',[-inf,0]};  % f \in ]0 +oo)

   % QSI epsilon
   eps = 0.1;
   parallel = true;  % sellect parallel execution or not

   % fstar parameters
   param.tol = 0.001;   % Tolerance
   param.eps = 0.001;   % Epsilon
   param.print = true;
   param.time = 20;    % limit computation time of fstar
   param.simpl = false;
   param.iter =10;       %Resultados parciales por pantalla cada "param.iter" iteraciones
   %param.jm = false;

   [S,U,N] = qsi(C,VARS,param,eps,parallel);    % call qsi
   toc
   S                                   %solution: red
   U                                   %undefined: yellow
   N                                   %non-solution: blue
   draw_boxes(S',U',N',1) ;          %plot boxes
   tic
return