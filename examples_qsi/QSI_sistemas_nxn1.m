function  QSI_sistemas_nxn1()
   clc
   tic

% QSI parameters
   eps = 0.01;
   parallel=true

% fstar parameters
   param.tol = 0.0001;   % Tolerance
   param.eps = 0.0001;   % Epsilon
   %param.print = true;
   param.print = false;
   param.time = 120;    % limit computation time of fstar
   param.simpl = false;
   param.iter =10;       %Resultados parciales por pantalla cada  "param.iter" iteraciones

%Ejemplo 12 de "IntLinInc3D package. User manual" Irene A. Sharaya

   eps = 0.02;
   VARS = [];
   VARS{numel(VARS)+1} = {'x1',[-2.5,2.5],'F'};
   VARS{numel(VARS)+1} = {'x2',[-2.5,2.5],'F'};
   VARS{numel(VARS)+1} = {'x3',[-2.5,2.5],'F'};
   VARS{numel(VARS)+1} = {'a11',[-1,1],'E'};
   VARS{numel(VARS)+1} = {'a12',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a13',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a21',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a22',[-1,1],'E'};
   VARS{numel(VARS)+1} = {'a23',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a31',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a32',[-2,2],'E'};
   VARS{numel(VARS)+1} = {'a33',[-1,1],'E'};
   VARS{numel(VARS)+1} = {'b1',[2,2],'E'};
   VARS{numel(VARS)+1} = {'b2',[2,2],'E'};
   VARS{numel(VARS)+1} = {'b3',[2,2],'E'};
   C = [];
   C{numel(C)+1} = {'a11*x1+a12*x2+a13*x3-b1',[0,0]};  % f \in [0 0]
   C{numel(C)+1} = {'a21*x1+a22*x2+a33*x3-b2',[0,0]};  % f \in [0 0]
   C{numel(C)+1} = {'a31*x1+a32*x2+a33*x3-b3',[0,0]};  % f \in [0 0]

%QSI del dominio
   [S,U,N] = qsi(C,VARS,param,eps,parallel);    % call qsi
   S;
   [fs,cs]=size(S)
   U;
   [fu,cu]=size(U)
   N;
   [fn,cn]=size(N)
   % S: Array of solution boxes
   % N: Non-solution boxes
   % U: Undefined boxes
   % X1 = S(:,1) % To extract first interval box of S
   % I1 = X1(1) % To extract first interval of the interval box X1


%Gr�fica
   %draw_boxes(S',U',N',1,2,3) ;% plot boxes (red:solution; blue:  non-solution; white: undefined)
   %draw_boxes(S',U',N',1,2); %boxes 2-D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes(U',S',N',1,2); %boxes 2-D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes(S',U',U',1,2); %boxes 2-D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes(U',S',U',1,2); %boxes 2-D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes(U',U',S',1,2); %boxes 2-D, true: red; undefined:  yellow; non-solution: blue
   draw_boxes(S',U',[],1,2,3); %boxes 3-D, true: red; undefined:  yellow; non-solution: blue
   draw_boxes(S',[],[],1,2,3); %boxes 3-D, true: red; undefined:  yellow; non-solution: blue
   draw_boxes([],U',[],1,2,3); %boxes 3-D, true: red; undefined:  yellow; non-solution: blue
   draw_boxes([],[],N',1,2,3); %boxes 3-D, true: red; undefined:  yellow; non-solution: blue
  %draw_boxes(S',U',N',1,2,3); %boxes 3D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes([],[],N',1,2,3); %boxes 3D, true: red; undefined:  yellow; non-solution: blue
   %draw_boxes(S',U',N',2,3);% grafica la proyecci�n sobre las  dimensiones 2 y 3.
%}
   toc
end