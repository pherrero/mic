function SISO()
clc

VARS{1} = {'k1',[18.3,36.1],'F'};   % free variable
VARS{2} = {'k2',[3.5,6],'F'};       % free variable
VARS{3} = {'q1',[0.4 0.55],'U'};     % universal variable
VARS{4} = {'q2',[0.49 0.55],'U'};     % universal variable
VARS{5} = {'q3',[0.46 0.55],'U'};     % universal variable
VARS{6} = {'c',[0.5,1],'E'};        % existential variable
VARS{7} = {'w',[0.5,2],'E'};        % existential variable

C{1}= {'w^2-(q1+q2+q3)*(q1+q3-k2)+q1^2*q2^2*q3-q2*q3*k1',[0,0]};

% QSI epsilon
eps = 0.1;

% fstar parameters
param.tol = 0.00001;   % Tolerance
param.eps = 0.000001;   % Epsilon
param.print = true;
param.time = 60;    % limit computation time of fstar
param.simpl = false;
param.iter =10;       %Resultados parciales por pantalla cada "param.iter" iteraciones
%param.inex = {'inn','ninc',[0,1.0e-6]};

tic
[S,U,N] = qsi(C,VARS,param,eps);    % call qsi
toc
S                                   %solution: red
U                                   %undefined: yellow
N                                   %non-solution: blue
figure
draw_boxes(S',U',N',1,2) ;          % plot boxes

return